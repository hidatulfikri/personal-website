# Personal Website

[![pipeline status](https://gitlab.com/hidatulfikri/personal-website/badges/master/pipeline.svg)](https://gitlab.com/hidatulfikri/personal-website/commits/master) 
[![coverage report](https://gitlab.com/hidatulfikri/personal-website/badges/master/coverage.svg)](https://gitlab.com/hidatulfikri/personal-website/commits/master)

This is a personal website reconstruction project based on Web Design & Programming Course opened in Faculty of Computer Science, Universitas Indonesia on Term 1 2018/2019.

## Access

* [Repository Link](https://gitlab.com/hidatulfikri/personal-website)
* [Heroku Link](http://datul.herokuapp.com)




## Author(s)

* **Hidayatul Fikri** - [hidatulfikri](https://gitlab.com/hidatulfikri)


## Acknowledgement

* Web Design & Programming Course Term 1 2018/2019, Faculty of Computer Science, Universitas Indonesia